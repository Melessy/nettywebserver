/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unused;

import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadJsonAsString 
{

    public static void main(String[] args) throws Exception 
    {
        String file = "src/test/resources/myFile.json";
        String json = readFileAsString(file);
        System.out.println(json);
    }
    
    public static String readFileAsString(String file)throws Exception
    {
        return new String(Files.readAllBytes(Paths.get(file)));
    }
}

